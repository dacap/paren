// (C) 2013 Kim, Taegyoon
// Paren language core

#pragma once
#ifndef LIBPAREN_H
#define LIBPAREN_H

#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <map>
#include <ctime>
#include <memory>

#define PAREN_VERSION "1.6"

namespace libparen {
    using namespace std;

	struct node;
	struct environment;
	typedef shared_ptr<node> snode;

    struct node {
        enum {T_NIL, T_INT, T_DOUBLE, T_BOOL, T_STRING, T_SYMBOL, T_LIST, T_BUILTIN, T_FN} type;
        enum builtin {PLUS, MINUS, MUL, DIV, CARET, PERCENT, SQRT, INC, DEC, PLUSPLUS, MINUSMINUS, FLOOR, CEIL, LN, LOG10, RAND,
            EQEQ, NOTEQ, LT, GT, LTE, GTE, ANDAND, OROR, NOT,
            IF, WHEN, FOR, WHILE,
            STRLEN, STRCAT, CHAR_AT, CHR,
            INT, DOUBLE, STRING, READ_STRING, TYPE, SET,
            EVAL, QUOTE, FN, LIST, APPLY, FOLD, MAP, FILTER, RANGE, NTH, LENGTH, BEGIN,
            PR, PRN, EXIT, SYSTEM, CONS, DEFMACRO, READ_LINE, SLURP, SPIT};
        union {
            int v_int;
            double v_double;
            bool v_bool;
			//node *p_node; // if T_SYMBOL, cached address of the data
			environment *outer_env; // if T_FN
        };
        string v_string;
        vector<snode> v_list;
		int code; // if T_SYMBOL, code for faster access		

        node();
        node(int a);
        node(double a);
        node(bool a);
        node(const string &a);
        node(const vector<snode> &a);

        int to_int(); // convert to int
        double to_double(); // convert to double
        string to_string(); // convert to string
        string type_str();
        string str_with_type();
    };

    struct environment {
        map<int, snode> env;
        environment *outer;
        environment();
        environment(environment *outer);
        snode get(int code);
		snode get(snode &k);
		snode set(snode &k, snode &v);
    };

    struct paren {
        paren();

        inline double rand_double();
        vector<string> tokenize(const string &s);
        vector<snode> parse(const string &s);

        environment global_env; // variables
                
        snode eval(snode &n, environment &env);        
        snode eval_all(vector<snode> &lst);
        snode compile(snode &n);
        vector<snode> compile_all(vector<snode> &lst);
        void print_logo();
        void prompt();
        void prompt2();
        inline void init();
        snode eval_string(string &s);
        snode eval_string(const char* s);
        inline void eval_print(string &s);
        void repl(); // read-eval-print loop

        snode get(const char* name);
        void set(const char* name, snode &value);
    }; // struct paren

	bool slurp(string filename, string &str);
	int spit(string filename, const string &str);
} // namespace libparen
#endif
