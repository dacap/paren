// (C) 2013 Kim, Taegyoon
// Paren language core

#include "libparen.h"
#include <algorithm>

namespace libparen {
    using namespace std;

    paren::paren() {
        init();
    }

	snode make_snode(const node& n) {
		return make_shared<node>(n);
	}

    node::node(): type(T_NIL) {}
    node::node(int a): type(T_INT), v_int(a) {}
    node::node(double a): type(T_DOUBLE), v_double(a) {}
    node::node(bool a): type(T_BOOL), v_bool(a) {}    
    node::node(const string &a): type(T_STRING), v_string(a) {}
    node::node(const vector<snode> &a): type(T_LIST), v_list(a) {}
	snode node_true(make_snode(node(true)));
	snode node_false(make_snode(node(false)));    
	snode node_0(make_snode(node(0)));
	snode node_1(make_snode(node(1)));
	snode nil(make_snode(node()));	

    inline int node::to_int() {
        switch (type) {
        case T_INT:
            return v_int;
        case T_DOUBLE:
            return (int) v_double;
        case T_BOOL:
            return (int) v_bool;
        case T_STRING:
            return atoi(v_string.c_str());
        default:
            return 0;
        }
    }

    inline double node::to_double() {
        switch (type) {
        case T_INT:
            return v_int;
        case T_DOUBLE:
            return v_double;
        case T_BOOL:
            return v_bool;
        case T_STRING:
            return atof(v_string.c_str());
        default:
            return 0.0;
        }
    }
    inline string node::to_string() {
		char buf[32];
		string ret;
        switch (type) {
        case T_NIL: break;
        case T_INT:
			sprintf(buf, "%d", v_int);
			ret = buf; break;
        case T_BUILTIN:
			sprintf(buf, "%d", v_int);
			ret = "builtin." + string(buf); break;
        case T_DOUBLE:
			sprintf(buf, "%.16g", v_double);
            ret = buf; break;
        case T_BOOL:
            return (v_bool ? "true" : "false");
        case T_STRING:
        case T_SYMBOL:
            return v_string;
        case T_FN:
        case T_LIST:
            {
                ret = '(';
                for (vector<snode>::iterator iter = v_list.begin(); iter != v_list.end(); iter++) {
                    if (iter != v_list.begin()) ret += ' ';
                    ret += (*iter)->to_string();
                }
                ret += ')';
                break;
            }
        }
		return ret;
    }
    inline string node::type_str() {
        switch (type) {
        case T_NIL:
            return "nil";
        case T_INT:
            return "int";
        case T_DOUBLE:
            return "double";
        case T_BOOL:
            return "bool";
        case T_STRING:
            return "string";
        case T_SYMBOL:
            return "symbol";
        case T_LIST:
            return "list";
        case T_BUILTIN:
            return "builtin";
        case T_FN:
            return "fn";
        default:
            return "invalid type";
        }
    }
    inline string node::str_with_type() {
        return to_string() + " : " + type_str();
    }

    inline double paren::rand_double() {
        return (double) rand() / ((double) RAND_MAX + 1.0);
    }

    class tokenizer {
    private:
        vector<string> ret;
        string acc; // accumulator
        string s;        
        void emit() { // add accumulated string to token list
            if (acc.length() > 0) {ret.push_back(acc); acc = "";}
        }
    public:
        int unclosed; // number of unclosed parenthesis ( or quotation "
        tokenizer(const string &s): s(s), unclosed(0) {}
        
        vector<string> tokenize() {
            int last = s.length() - 1;
            for (int pos=0; pos <= last; pos++) {
                char c = s.at(pos);
                if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
                    emit();
                }
				else if (c == ';' || c == '#') { // end-of-line comment
                    emit();
                    do pos++; while (pos <= last && s.at(pos) != '\n');
                }
                else if (c == '"') { // beginning of string
                    unclosed++;
                    emit();
                    acc += '"';
                    pos++;
                    while (pos <= last) {
                        if (s.at(pos) == '"') {unclosed--; break;}
                        if (s.at(pos) == '\\') { // escape
                            char next = s.at(pos+1);
                            if (next == 'r') next = '\r';
                            else if (next == 'n') next = '\n';
                            else if (next == 't') next = '\t';
                            acc += next;
                            pos += 2;
                        }
                        else {
                            acc += s.at(pos);
                            pos++;
                        }
                    }
                    emit();
                }
                else if (c == '(') {
                    unclosed++;
                    emit();
                    acc += c;
                    emit();
                }
                else if (c == ')') {
                    unclosed--;
                    emit();
                    acc += c;
                    emit();
                }
                else {
                    acc += c;
                }
            }
            emit();
            return ret;
        }
    };

    vector<string> paren::tokenize(const string &s) {
        return tokenizer(s).tokenize();
    }
    
    map<string, int> symcode;
    vector<string> symname;
    int ToCode(const string &name) {
        int r;
		map<string, int>::iterator found = symcode.find(name);
		if (found != symcode.end()) {
			return found->second;
		} else {
			r = symcode.size();
			symcode[name] = r;
			symname.push_back(name);
			return r;
		}        
    }

    class parser {        
    private:
        int pos;
        vector<string> tokens;
    public:
        parser(const vector<string> &tokens): pos(0), tokens(tokens) {}
        vector<snode> parse() {
            vector<snode> ret;
            int last = tokens.size() - 1;
            for (;pos <= last; pos++) {
                string tok = tokens.at(pos);
                if (tok.at(0) == '"') { // double-quoted string
                    ret.push_back(make_snode(tok.substr(1)));
                }
                else if (tok == "(") { // list
                    pos++;
                    ret.push_back(make_snode(parse()));
                }
                else if (tok == ")") { // end of list
                    break;
                }
                else if (isdigit(tok.at(0)) || (tok.at(0) == '-' && tok.length() >= 2 && isdigit(tok.at(1)))) { // number
                    if (tok.find('.') != string::npos || tok.find('e') != string::npos) { // double
                        ret.push_back(make_snode(atof(tok.c_str())));
                    } else {
                        ret.push_back(make_snode(atoi(tok.c_str())));
                    }
                } else { // symbol
                    node n;
                    n.type = node::T_SYMBOL;
                    n.v_string = tok;
//					n.p_node = NULL;
					n.code = ToCode(tok);
                    ret.push_back(make_snode(n));
                }
            }
            return ret;
        }        
    };
    
    vector<snode> paren::parse(const string &s) {
        return parser(tokenize(s)).parse();
    }

    environment::environment(): outer(NULL) {}
    environment::environment(environment *outer): outer(outer) {}    

    snode environment::get(int code) {
        map<int, snode>::iterator found = env.find(code);
        if (found != env.end()) {
            return found->second;
        }
        else {
            if (outer != NULL) {
                return outer->get(code);
            }
            else {
                return nil;
            }
        }
    }

	snode environment::get(snode &k) {
		return get(k->code);
		//if (k->p_node != NULL) { // cached
		//	return make_snode(*k->p_node);
		//} else {
		//	return make_snode(*(k->p_node = get(k->code).get())); // make cache
		//}
	}

	snode environment::set(snode &k, snode &v) {
		return env[k->code] = v;
		//if (k->p_node != NULL) { // cached
		//	*k->p_node = *v;
		//} else {
		//	k->p_node = (env[k->code] = v).get(); // make cache
		//}
		//return make_snode(*k->p_node);
	}

    snode builtin(int b) {
        snode n(make_snode(b));
        n->type = node::T_BUILTIN;
        return n;
    }

    snode fn(snode n, environment *outer_env) {
        snode n2(n);
        n2->type = node::T_FN;
		n2->outer_env = outer_env;
        return n2;
    }

    map<string, vector<snode> > macros;

    snode apply_macro(snode body, map<string, snode> vars) {
        if (body->type == node::T_LIST) {            
            vector<snode> &bvec = body->v_list;
            vector<snode> ret;            
            for (unsigned int i = 0; i < bvec.size(); i++) {
                snode b = bvec.at(i);
                if (b->v_string == "...") {
                    snode &vargs = vars[b->v_string];
                    ret.insert(ret.end(), vargs->v_list.begin(), vargs->v_list.end());                    
                } else ret.push_back(apply_macro(bvec.at(i), vars));
            }
            return make_snode(ret);
        } else {
            string &bstr = body->v_string;
            if (vars.find(bstr) != vars.end()) return vars[bstr]; else return body;
        }
    }

    snode macroexpand(snode n) {        
        vector<snode> nList = n->v_list;
        if (macros.find(nList[0]->v_string) != macros.end()) {            
            vector<snode> &macro = macros[nList[0]->v_string];
            map<string, snode> macrovars;
            vector<snode> &argsyms = macro[0]->v_list;            
            for (unsigned int i = 0; i < argsyms.size(); i++) {
                string argsym = argsyms.at(i)->v_string;
                if (argsym == "...") {
                    vector<snode> ellipsis;
                    snode n2(make_snode(ellipsis));
                    vector<snode> &ellipsis2 = n2->v_list;
                    for (unsigned int i2 = i + 1; i2 < nList.size(); i2++)
                        ellipsis2.push_back(nList.at(i2));
                    macrovars[argsym] = n2;
                    break;
                } else {
                    macrovars[argsyms.at(i)->v_string] = nList.at(i+1);                    
                }
            }
            return apply_macro(macro[1], macrovars);
        } else
            return n;
    }

    snode paren::compile(snode &n) {
        switch (n->type) {
        case node::T_LIST: // function (FUNCTION ARGUMENT ..)
            {
				if (n->v_list.size() == 0) return nil;
                snode func = compile(n->v_list[0]);                
                if (func->type == node::T_SYMBOL && func->v_string == "defmacro") { // (defmacro add (a b) (+ a b)) ; define macro
                    vector<snode> v;
                    v.push_back(n->v_list[2]);
                    v.push_back(n->v_list[3]);                        
                    macros[n->v_list[1]->v_string] = v;
                    return nil;
                } else {
                    if (macros.find(func->v_string) != macros.end()) {
						snode expanded = macroexpand(n);
                        return compile(expanded);
                    } else {
                        vector<snode> r;
                        for (vector<snode>::iterator i = n->v_list.begin(); i != n->v_list.end(); i++) {
                            r.push_back(compile(*i));
                        }
                        return make_snode(r);
                    } 
                }
            }
		default: return n;
        }
    }

    snode paren::eval(snode &n, environment &env) {
        switch (n->type) {
        case node::T_NIL:
        case node::T_INT:
        case node::T_DOUBLE:
        case node::T_BOOL:
        case node::T_STRING:
        case node::T_BUILTIN:
        case node::T_FN:
            {
                return n;
            }
        case node::T_SYMBOL:
            {
				snode n2 = env.get(n);
                if (n2->type != node::T_NIL) {
                    return n2;
				}
                else {
                    cerr << "Unknown variable: " << n->v_string << endl;
                    return nil;
                }
                
            }
        case node::T_LIST: // function (FUNCTION ARGUMENT ..)
            {
                node &func = *eval(n->v_list[0], env);
                int builtin = -1;
                if (func.type == node::T_BUILTIN) {
                    builtin = func.v_int;
                    switch(builtin) {
                        case node::PLUS: // (+ X ..)
                            {
                                int len = n->v_list.size();
                                if (len <= 1) return node_0;
                                snode first = eval(n->v_list[1], env);
                                if (first->type == node::T_INT) {
                                    int sum = first->v_int;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum += eval(*i, env)->to_int();
                                    }
                                    return make_snode(sum);
                                }
                                else {
                                    double sum = first->v_double;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum += eval(*i, env)->to_double();
                                    }
                                    return make_snode(sum);
                                }
                            }
                        case node::MINUS: // (- X ..)
                            {
                                int len = n->v_list.size();
                                if (len <= 1) return node_0;
                                node &first = *eval(n->v_list[1], env);
                                if (first.type == node::T_INT) {
                                    int sum = first.v_int;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum -= eval(*i, env)->to_int();
                                    }
                                    return make_snode(sum);
                                }
                                else {
                                    double sum = first.v_double;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum -= eval(*i, env)->to_double();
                                    }
                                    return make_snode(sum);
                                }
                            }
                        case node::MUL: // (* X ..)
                            {
                                int len = n->v_list.size();
                                if (len <= 1) return node_1;
                                node &first = *eval(n->v_list[1], env);
                                if (first.type == node::T_INT) {
                                    int sum = first.v_int;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum *= eval(*i, env)->to_int();
                                    }
                                    return make_snode(sum);
                                }
                                else {
                                    double sum = first.v_double;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        sum *= eval(*i, env)->to_double();
                                    }
                                    return make_snode(sum);
                                }
                            }
                        case node::DIV: // (/ X ..)
                            {
                                int len = n->v_list.size();
                                if (len <= 1) return node_1;
                                node &first = *eval(n->v_list[1], env);
                                if (first.type == node::T_INT) {
                                    int acc = first.v_int;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        acc /= eval(*i, env)->to_int();
                                    }
                                    return make_snode(acc);
                                }
                                else {
                                    double acc = first.v_double;
                                    for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                        acc /= eval(*i, env)->to_double();
                                    }
                                    return make_snode(acc);
                                }
                            }
                        case node::CARET: { // (^ BASE EXPONENT)
                            return make_snode(pow(eval(n->v_list[1], env)->to_double(), eval(n->v_list[2], env)->to_double()));}
                        case node::PERCENT: { // (% DIVIDEND DIVISOR)
                            return make_snode(eval(n->v_list[1], env)->to_int() % eval(n->v_list[2], env)->to_int());}
                        case node::SQRT: { // (sqrt X)
                            return make_snode(sqrt(eval(n->v_list[1], env)->to_double()));}
                        case node::INC: { // (inc X)
                                int len = n->v_list.size();
                                if (len <= 1) return make_snode(0);
                                node &first = *eval(n->v_list[1], env);
                                if (first.type == node::T_INT) {
                                    return make_snode(first.v_int + 1);
                                }
                                else {
                                    return make_snode(first.v_double + 1.0);
                                }
                            }
                        case node::DEC: { // (dec X)
                                int len = n->v_list.size();
                                if (len <= 1) return make_snode(0);
                                node &first = *eval(n->v_list[1], env);
                                if (first.type == node::T_INT) {
                                    return make_snode(first.v_int - 1);
                                }
                                else {
                                    return make_snode(first.v_double - 1.0);
                                }
                            }
                        case node::PLUSPLUS: { // (++ X)
                                int len = n->v_list.size();
                                if (len <= 1) return make_snode(0);
								snode first = env.get(n->v_list[1]);
                                if (first->type == node::T_INT) {
									first->v_int++;
                                    return first;
                                }
                                else {
									first->v_double++;
                                    return first;
                                }
                            }
                        case node::MINUSMINUS: { // (-- X)
                                int len = n->v_list.size();
                                if (len <= 1) return make_snode(0);
								snode first = env.get(n->v_list[1]);
                                if (first->type == node::T_INT) {
									first->v_int--;
									return first;
                                }
                                else {
									first->v_double--;
									return first;
                                }
                            }
                        case node::FLOOR: { // (floor X)
                            return make_snode(floor(eval(n->v_list[1], env)->to_double()));}
                        case node::CEIL: { // (ceil X)
                            return make_snode(ceil(eval(n->v_list[1], env)->to_double()));}
                        case node::LN: { // (ln X)
                            return make_snode(log(eval(n->v_list[1], env)->to_double()));}
                        case node::LOG10: { // (log10 X)
                            return make_snode(log10(eval(n->v_list[1], env)->to_double()));}
                        case node::RAND: { // (rand)
                            return make_snode(rand_double());}
                        case node::SET: // (set SYMBOL VALUE)
                            {
								snode v = make_snode(*eval(n->v_list[2], env));
								return env.set(n->v_list[1], v);
                            }
                        case node::EQEQ: { // (== X ..) short-circuit                    
                            snode first = eval(n->v_list[1], env);
                            if (first->type == node::T_INT) {
                                int firstv = first->v_int;
                                for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                    if (eval(*i, env)->to_int() != firstv) {return make_snode(false);}
                                }
                            }
                            else {
                                double firstv = first->v_double;
                                for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                    if (eval(*i, env)->to_double() != firstv) {return make_snode(false);}
                                }
                            }
                            return node_true;}
                        case node::NOTEQ: { // (!= X ..) short-circuit                    
                            snode first = eval(n->v_list[1], env);
                            if (first->type == node::T_INT) {
                                int firstv = first->v_int;
                                for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                    if (eval(*i, env)->to_int() == firstv) {return make_snode(false);}
                                }
                            }
                            else {
                                double firstv = first->v_double;
                                for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                    if (eval(*i, env)->to_double() == firstv) {return make_snode(false);}
                                }
                            }
                            return node_true;}
                        case node::LT: { // (< X Y)
                            snode first = eval(n->v_list[1], env);
                            snode second = eval(n->v_list[2], env);
                            if (first->type == node::T_INT) {
                                return make_snode(first->v_int < second->to_int());
                            }
                            else {
                                return make_snode(first->v_double < second->to_double());
                            }}
                        case node::GT: { // (> X Y)
                            snode first = eval(n->v_list[1], env);
                            snode second = eval(n->v_list[2], env);
                            if (first->type == node::T_INT) {
                                return make_snode(first->v_int > second->to_int());
                            }
                            else {
                                return make_snode(first->v_double > second->to_double());
                            }}
                        case node::LTE: { // (<= X Y)
                            snode first = eval(n->v_list[1], env);
                            snode second = eval(n->v_list[2], env);
                            if (first->type == node::T_INT) {
                                return make_snode(first->v_int <= second->to_int());
                            }
                            else {
                                return make_snode(first->v_double <= second->to_double());
                            }}
                        case node::GTE: { // (>= X Y)
                            snode first = eval(n->v_list[1], env);
                            snode second = eval(n->v_list[2], env);
                            if (first->type == node::T_INT) {
                                return make_snode(first->v_int >= second->to_int());
                            }
                            else {
                                return make_snode(first->v_double >= second->to_double());
                            }}
                        case node::ANDAND: { // (&& X ..) short-circuit
                            for (vector<snode>::iterator i = n->v_list.begin() + 1; i != n->v_list.end(); i++) {
                                if (!eval(*i, env)->v_bool) {return node_false;}
                            }
                            return node_true;}
                        case node::OROR: { // (|| X ..) short-circuit
                            for (vector<snode>::iterator i = n->v_list.begin() + 1; i != n->v_list.end(); i++) {
                                if (eval(*i, env)->v_bool) {return node_true;}
                            }
                            return node_false;}
                        case node::NOT: { // (! X)
                            return make_snode(!(eval(n->v_list[1], env)->v_bool));}
                        case node::IF: { // (if CONDITION THEN_EXPR ELSE_EXPR)
                            snode &cond = n->v_list[1];
                            if (eval(cond, env)->v_bool) {
                                return eval(n->v_list[2], env);
                            }
                            else {
                                return eval(n->v_list[3], env);
                            }}
                        case node::WHEN: { // (when CONDITION EXPR ..)
                            snode &cond = n->v_list[1];
                            if (eval(cond, env)->v_bool) {
                                int len = n->v_list.size();
                                for (int i = 2; i < len - 1; i++) {
                                    eval(n->v_list[i], env);
                                }
                                return eval(n->v_list[len - 1], env); // returns last EXPR
                            }
                            return nil;}
                        case node::FOR: // (for SYMBOL START END STEP EXPR ..)
                            {
                                node start = *eval(n->v_list[2], env);
								snode sstart = make_snode(start);
                                //env.env[n->v_list[1].v_string] = start;
								env.set(n->v_list[1], sstart);
                                int len = n->v_list.size();
                                if (start.type == node::T_INT) {                            
                                    int last = eval(n->v_list[3], env)->to_int();
                                    int step = eval(n->v_list[4], env)->to_int();                                
									int &a = env.get(n->v_list[1])->v_int;
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            for (int i = 5; i < len; i++) {
                                                eval(n->v_list[i], env);
                                            }
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            for (int i = 5; i < len; i++) {
                                                eval(n->v_list[i], env);
                                            }
                                        }
                                    }
                                }
                                else {
                                    double last = eval(n->v_list[3], env)->to_double();
                                    double step = eval(n->v_list[4], env)->to_double();                                
									double &a = env.get(n->v_list[1])->v_double;
                                    if (step >= 0) {
                                        for (; a <= last; a += step) {
                                            for (int i = 5; i < len; i++) {
                                                eval(n->v_list[i], env);
                                            }
                                        }
                                    }
                                    else {
                                        for (; a >= last; a += step) {
                                            for (int i = 5; i < len; i++) {
                                                eval(n->v_list[i], env);
                                            }
                                        }
                                    }
                                }
                                return nil;
                            }
                        case node::WHILE: { // (while CONDITION EXPR ..)
                            snode &cond = n->v_list[1];
                            int len = n->v_list.size();
                            while (eval(cond, env)->v_bool) {
                                for (int i = 2; i < len; i++) {
                                    eval(n->v_list[i], env);
                                }
                            }
                            return nil; }
                        case node::STRLEN: { // (strlen X)
                            return make_snode((int) eval(n->v_list[1], env)->v_string.size());}
                        case node::STRCAT: { // (strcat X ..)
                            int len = n->v_list.size();
                            if (len <= 1) return make_snode("");
                            snode first = eval(n->v_list[1], env);
                            string acc = first->to_string();
                            for (vector<snode>::iterator i = n->v_list.begin() + 2; i != n->v_list.end(); i++) {
                                acc += eval(*i, env)->to_string();
                            }
                            return make_snode(acc);}
                        case node::CHAR_AT: { // (char-at X)
                            return make_snode(eval(n->v_list[1], env)->v_string[eval(n->v_list[2], env)->to_int()]);}
                        case node::CHR: { // (chr X)
                            char temp[2] = " ";
                            temp[0] = (char) eval(n->v_list[1], env)->to_int();
                            return make_snode(string(temp));}
                        case node::STRING: { // (string X)
                            return make_snode(eval(n->v_list[1], env)->to_string());}
                        case node::DOUBLE: { // (double X)
                            return make_snode(eval(n->v_list[1], env)->to_double());}
                        case node::INT: { // (int X)
                            return make_snode(eval(n->v_list[1], env)->to_int());}
                        case node::READ_STRING: { // (read-string X)
                            return parse(eval(n->v_list[1], env)->to_string())[0];}
                        case node::TYPE: { // (type X)
                            return make_snode(eval(n->v_list[1], env)->type_str());}
                        case node::EVAL: { // (eval X)                    
                            snode n2 = eval(n->v_list[1], env);
                            return eval(n2, env);}
                        case node::QUOTE: { // (quote X)
                            return n->v_list[1];}
                        case node::FN: { // (fn (ARGUMENT ..) BODY) => lexical closure
                            snode n2 = fn(make_snode(n->v_list), &env);
                            return n2;}
                        case node::LIST: { // (list X ..)
                            vector<snode> ret;
                            for (unsigned int i = 1; i < n->v_list.size(); i++) {
                                ret.push_back(eval(n->v_list[i], env));
                            }
                            return make_snode(ret);}
                        case node::APPLY: { // (apply FUNC LIST)
                            vector<snode> expr;
                            snode f = eval(n->v_list[1], env);
                            expr.push_back(f);
                            vector<snode> lst = eval(n->v_list[2], env)->v_list;
                            for (unsigned int i = 0; i < lst.size(); i++) {
                                expr.push_back(lst.at(i));
                            }
                            snode n2 = make_snode(expr);
                            return eval(n2, env);
                        }
                        case node::FOLD: { // (fold FUNC LIST)
                            snode f = eval(n->v_list.at(1), env);
                            vector<snode> lst = eval(n->v_list.at(2), env)->v_list;
                            snode acc = eval(lst.at(0), env);
                            vector<snode> expr; // (FUNC ITEM)
                            expr.push_back(f);
                            expr.push_back(nil); // first argument
                            expr.push_back(nil); // second argument
                            for (unsigned int i = 1; i < lst.size(); i++) {
                                expr[1] = acc;
                                expr[2] = lst.at(i);
                                snode n2 = make_snode(expr);
                                acc = eval(n2, env);
                            }
                            return acc;
                        }
                        case node::MAP: { // (map FUNC LIST)
                            snode f = eval(n->v_list.at(1), env);
                            vector<snode> lst = eval(n->v_list.at(2), env)->v_list;
                            vector<snode> acc;
                            vector<snode> expr; // (FUNC ITEM)
                            expr.push_back(f);
                            expr.push_back(nil);
                            for (unsigned int i = 0; i < lst.size(); i++) {
                                expr[1] = lst.at(i);
                                snode n2 = make_snode(expr);
                                acc.push_back(eval(n2, env));
                            }                    
                            return make_snode(acc);
                        }
                        case node::FILTER: { // (filter FUNC LIST)
                            snode f = eval(n->v_list.at(1), env);
                            vector<snode> lst = eval(n->v_list.at(2), env)->v_list;
                            vector<snode> acc;
                            vector<snode> expr; // (FUNC ITEM)
                            expr.push_back(f);
                            expr.push_back(nil);
                            for (unsigned int i = 0; i < lst.size(); i++) {
                                snode item = lst.at(i);
                                expr[1] = item;
                                snode n2 = make_snode(expr);
                                snode ret = eval(n2, env);
                                if (ret->v_bool) acc.push_back(item);
                            }                    
                            return make_snode(acc);
                        }
                        case node::RANGE: { // (range START END STEP)
                            node start = *eval(n->v_list.at(1), env);                    
                            vector<snode> ret;
                            if (start.type == node::T_INT) {
                                int a = eval(n->v_list.at(1), env)->to_int();
                                int last = eval(n->v_list.at(2), env)->to_int();
                                int step = eval(n->v_list.at(3), env)->to_int();
                                if (step >= 0) {
                                    for (; a <= last; a += step) {
                                        ret.push_back(make_snode(a));}}
                                else {
                                    for (; a >= last; a += step) {
                                        ret.push_back(make_snode(a));}}
                            }
                            else {
                                double a = eval(n->v_list.at(1), env)->to_double();
                                double last = eval(n->v_list.at(2), env)->to_double();
                                double step = eval(n->v_list.at(3), env)->to_double();
                                if (step >= 0) {
                                    for (; a <= last; a += step) {
                                        ret.push_back(make_snode(a));}}
                                else {
                                    for (; a >= last; a += step) {
                                        ret.push_back(make_snode(a));}}
                            }
                            return make_snode(ret);
                        }
                        case node::NTH: { // (nth INDEX LIST)
                            int i = eval(n->v_list.at(1), env)->to_int();
                            vector<snode> &lst = eval(n->v_list.at(2), env)->v_list;
                            return lst.at(i);}
                        case node::LENGTH: { // (length LIST)                    
                            const vector<snode> &lst = eval(n->v_list.at(1), env)->v_list;
                            return make_snode((int) lst.size());}
                        case node::BEGIN: { // (begin X ..)                    
                            int last = n->v_list.size() - 1;
                            if (last <= 0) return nil;
                            for (int i = 1; i < last; i++) {
                                eval(n->v_list[i], env);
                            }
                            return eval(n->v_list[last], env);}
                        case node::PR: // (pr X ..)
                            {
                                vector<snode>::iterator first = n->v_list.begin() + 1;
                                for (vector<snode>::iterator i = first; i != n->v_list.end(); i++) {
                                    if (i != first) printf(" ");
                                    printf("%s", eval(*i, env)->to_string().c_str());
                                }
                                return nil;
                            }
                        case node::PRN: // (prn X ..)
                            {
                                vector<snode>::iterator first = n->v_list.begin() + 1;
                                for (vector<snode>::iterator i = first; i != n->v_list.end(); i++) {
                                    if (i != first) printf(" ");
                                    printf("%s", eval(*i, env)->to_string().c_str());
                                }
                                puts("");
                                return nil;
                            }
                        case node::EXIT: { // (exit X)
                                puts("");
                                exit(eval(n->v_list[1], env)->to_int());
                                return nil; }
                        case node::SYSTEM: { // Invokes the command processor to execute a command.
                            string cmd;
                            for (unsigned int i = 1; i < n->v_list.size(); i++) {
                                if (i != 1) cmd += ' ';
                                cmd += eval(n->v_list[i], env)->v_string;
                            }
                            return make_snode(system(cmd.c_str()));}
                        case node::CONS: { // (cons x lst): Returns a new list where x is the first element and lst is the rest.
                            snode x = eval(n->v_list[1], env);
                            vector<snode> lst = eval(n->v_list[2], env)->v_list;
                            vector<snode> r;
                            r.push_back(x);
                            for (vector<snode>::iterator i = lst.begin(); i != lst.end(); i++) {
                                r.push_back(*i);
                            }
                            return make_snode(r);
                        }
						case node::READ_LINE: { // (read-line)
							string line;
							if (!getline(cin, line)) { // EOF								
								return nil;
							} else {
								return make_snode(line);
							}
						}
						case node::SLURP: { // (slurp FILENAME)
							string filename = eval(n->v_list[1], env)->v_string;
							string str;
							if (slurp(filename, str))
								return make_snode(str);
							else
								return nil;
						}
						case node::SPIT: { // (spit FILENAME STRING)
							string filename = eval(n->v_list[1], env)->v_string;
							string str = eval(n->v_list[2], env)->v_string;
							return make_snode(spit(filename, str));
						}
                        default: {
                            cerr << "Not implemented function: [" << func.v_string << "]" << endl;
                            return nil;}
                    } // end switch
                }
                else {
                    vector<snode> &f = func.v_list;
                    if (func.type == node::T_FN) {
                        // anonymous function application-> lexical scoping
                        // (fn (ARGUMENT ..) BODY ..)
                        vector<snode> &arg_syms = f[1]->v_list;

						environment local_env(func.outer_env);
                        int alen = arg_syms.size();
                        for (int i=0; i<alen; i++) { // assign arguments
                            //string &k = arg_syms.at(i)->v_string;
							//local_env.env[k] = eval(n->v_list.at(i+1), env);
							snode &k = arg_syms[i];
							local_env.env[k->code] = eval(n->v_list.at(i+1), env);
                        }

                        int flen = f.size();
                        for (int i=2; i<flen-1; i++) { // body
                            eval(f.at(i), local_env);
                        }
                        snode ret = eval(f.at(flen-1), local_env);
                        return ret;
                    }
                    else {
                        cerr << "Unknown function: [" << func.to_string() << "]" << endl;
                        return nil;
                    }
                }
            }
        default:
            cerr << "Unknown type" << endl;
            return nil;
        }
    }

    vector<snode> paren::compile_all(vector<snode> &lst) {
        vector<snode> compiled;
        int last = lst.size() - 1;        
        for (int i = 0; i <= last; i++) {
            compiled.push_back(compile(lst[i]));
        }
        return compiled;
    }

    snode paren::eval_all(vector<snode> &lst) {
        int last = lst.size() - 1;
        if (last < 0) return nil;
        for (int i = 0; i < last; i++) {
            eval(lst[i], global_env);
        }
        return eval(lst[last], global_env);
    }
        
    template <typename T>
    void print_map_keys(map<string, T> &m) {
        int i=0;
        for (typename map<string, T>::iterator iter = m.begin(); iter != m.end(); iter++) {
            printf(" %s", iter->first.c_str());
            i++;
            if (i % 10 == 0) puts("");
        }
        puts("");
    }
    
    void paren::print_logo() {
        printf("Paren %s (C) 2013 Kim, Taegyoon (https://bitbucket.org/ktg/paren)\n", PAREN_VERSION);        
        puts("Predefined Symbols:");
        //print_map_keys(global_env.env);
		vector<string> v;
		for (map<int, snode>::iterator iter = global_env.env.begin(); iter != global_env.env.end(); iter++) {			
			v.push_back(symname[iter->first]);
		}
		sort(v.begin(), v.end());
		for (vector<string>::iterator iter = v.begin(); iter != v.end(); iter++) {
			printf(" %s", (*iter).c_str());
		}
		puts("");

		puts("Macros:");
        print_map_keys(macros);
    } 

    void paren::prompt() {
        printf("> ");
    }

    void paren::prompt2() {
        printf("  ");
    }

    inline void paren::init() {
        srand((unsigned int) time(0));
        global_env.env[ToCode("true")] = make_snode(true);
        global_env.env[ToCode("false")] = make_snode(false);
        global_env.env[ToCode("E")] = make_snode(2.71828182845904523536);
        global_env.env[ToCode("PI")] = make_snode(3.14159265358979323846);
                
        global_env.env[ToCode("+")] = builtin(node::PLUS);
        global_env.env[ToCode("-")] = builtin(node::MINUS);
        global_env.env[ToCode("*")] = builtin(node::MUL);
        global_env.env[ToCode("/")] = builtin(node::DIV);
        global_env.env[ToCode("^")] = builtin(node::CARET);
        global_env.env[ToCode("%")] = builtin(node::PERCENT);
        global_env.env[ToCode("sqrt")] = builtin(node::SQRT);
        global_env.env[ToCode("inc")] = builtin(node::INC);
        global_env.env[ToCode("dec")] = builtin(node::DEC);
        global_env.env[ToCode("++")] = builtin(node::PLUSPLUS);
        global_env.env[ToCode("--")] = builtin(node::MINUSMINUS);
        global_env.env[ToCode("floor")] = builtin(node::FLOOR);
        global_env.env[ToCode("ceil")] = builtin(node::CEIL);
        global_env.env[ToCode("ln")] = builtin(node::LN);
        global_env.env[ToCode("log10")] = builtin(node::LOG10);
        global_env.env[ToCode("rand")] = builtin(node::RAND);
        global_env.env[ToCode("==")] = builtin(node::EQEQ);
        global_env.env[ToCode("!=")] = builtin(node::NOTEQ);
        global_env.env[ToCode("<")] = builtin(node::LT);
        global_env.env[ToCode(">")] = builtin(node::GT);
        global_env.env[ToCode("<=")] = builtin(node::LTE);
        global_env.env[ToCode(">=")] = builtin(node::GTE);
        global_env.env[ToCode("&&")] = builtin(node::ANDAND);
        global_env.env[ToCode("||")] = builtin(node::OROR);
        global_env.env[ToCode("!")] = builtin(node::NOT);
        global_env.env[ToCode("if")] = builtin(node::IF);
        global_env.env[ToCode("when")] = builtin(node::WHEN);
        global_env.env[ToCode("for")] = builtin(node::FOR);
        global_env.env[ToCode("while")] = builtin(node::WHILE);
        global_env.env[ToCode("strlen")] = builtin(node::STRLEN);
        global_env.env[ToCode("strcat")] = builtin(node::STRCAT);
        global_env.env[ToCode("char-at")] = builtin(node::CHAR_AT);
        global_env.env[ToCode("chr")] = builtin(node::CHR);
        global_env.env[ToCode("int")] = builtin(node::INT);
        global_env.env[ToCode("double")] = builtin(node::DOUBLE);
        global_env.env[ToCode("string")] = builtin(node::STRING);
        global_env.env[ToCode("read-string")] = builtin(node::READ_STRING);
        global_env.env[ToCode("type")] = builtin(node::TYPE);
        global_env.env[ToCode("eval")] = builtin(node::EVAL);
        global_env.env[ToCode("quote")] = builtin(node::QUOTE);
        global_env.env[ToCode("fn")] = builtin(node::FN);
        global_env.env[ToCode("list")] = builtin(node::LIST);
        global_env.env[ToCode("apply")] = builtin(node::APPLY);
        global_env.env[ToCode("fold")] = builtin(node::FOLD);
        global_env.env[ToCode("map")] = builtin(node::MAP);
        global_env.env[ToCode("filter")] = builtin(node::FILTER);
        global_env.env[ToCode("range")] = builtin(node::RANGE);
        global_env.env[ToCode("nth")] = builtin(node::NTH);
        global_env.env[ToCode("length")] = builtin(node::LENGTH);
        global_env.env[ToCode("begin")] = builtin(node::BEGIN);
        global_env.env[ToCode("set")] = builtin(node::SET);
        global_env.env[ToCode("pr")] = builtin(node::PR);
        global_env.env[ToCode("prn")] = builtin(node::PRN);
        global_env.env[ToCode("exit")] = builtin(node::EXIT);
        global_env.env[ToCode("system")] = builtin(node::SYSTEM);
        global_env.env[ToCode("cons")] = builtin(node::CONS);
        global_env.env[ToCode("defmacro")] = builtin(node::DEFMACRO);
		global_env.env[ToCode("read-line")] = builtin(node::READ_LINE);
		global_env.env[ToCode("slurp")] = builtin(node::SLURP);
		global_env.env[ToCode("spit")] = builtin(node::SPIT);

        eval_string("(defmacro setfn (name ...) (set name (fn ...)))");
		eval_string("(defmacro defn (...) (setfn ...))");
    }

    snode paren::eval_string(string &s) {
        vector<snode> vec = parse(s);
		vector<snode> compiled = compile_all(vec);
        return eval_all(compiled);
    }

    snode paren::eval_string(const char* s) {
        string s2(s);
        return eval_string(s2);
    }

    inline void paren::eval_print(string &s) {
		puts(eval_string(s)->str_with_type().c_str());
    }

    // read-eval-print loop
    void paren::repl() {
        string code;
        while (true) {
            if (code.length() == 0) prompt(); else prompt2();
            string line;
            if (!getline(cin, line)) { // EOF
                eval_print(code);
                return;
            }
            code += '\n' + line;
            tokenizer t(code);
            t.tokenize();
            if (t.unclosed <= 0) { // no unmatched parenthesis nor quotation
                eval_print(code);
                code = "";
            }
        }
    }

    snode paren::get(const char* name) {
        string s(name);
        return global_env.get(ToCode(s));
    }

    void paren::set(const char* name, snode &value) {
        string s(name);
        global_env.env[ToCode(s)] = value;
    }

	// extracts characters from filename and stores them into str
	bool slurp(string filename, string &str) {        
		FILE *file = fopen(filename.c_str(), "r");
		if (file != NULL) {
			fseek(file, 0, SEEK_END);
			long size = ftell(file);
			fseek(file, 0, SEEK_SET);
			char *buf = (char *) malloc((size + 1) * sizeof(char)); // size + null
			buf[size] = 0;
			fread(buf, sizeof(char), size, file);
			fclose(file);

			str = string(buf, size);
			free(buf);
			return true;
		}
		else {
			return false;
		}
	}

	// Opposite of slurp. Writes str to filename.
	int spit(string filename, const string &str) {
		FILE *file = fopen(filename.c_str(), "w");
		if (file != NULL) {
			int written = fwrite(str.data(), sizeof(char), str.length(), file);
			fclose(file);
			return written;		
		}
		else {
			return -1;
		}
	}
} // namespace libparen
